module gitlab.com/fastogt/gofastocloud_http

go 1.19

require (
	github.com/gorilla/websocket v1.5.0
	gitlab.com/fastogt/gofastocloud v1.11.18
	gitlab.com/fastogt/gofastocloud_base v1.10.9
	gitlab.com/fastogt/gofastogt v1.7.9
)

require golang.org/x/sys v0.5.0 // indirect
