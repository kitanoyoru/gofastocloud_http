package unittests

import (
	"encoding/json"
	"testing"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_http/gofastocloud_http/public"
)

func TestUnmarshalJSON(t *testing.T) {
	var testJSON []byte

	//  test StartStreamResponse
	var start public.StartStreamResponse
	testJSON = []byte(`{"data":null}`)
	err := json.Unmarshal(testJSON, &start)
	AssertTrue(t, err == nil)
	AssertEqual(t, start, public.StartStreamResponse{})

	//  test VersionResponse
	var version public.VersionResponse
	testJSON = []byte(`{"data":{"version":{"short":"1.23.4", "human_readable":"1.23.4 Revision: aa1f615c"}}}`)
	err = json.Unmarshal(testJSON, &version)
	AssertTrue(t, err == nil)
	AssertEqual(t, version, public.VersionResponse{Data: public.Version{Version: public.VersionStruct{Short: "1.23.4", HumanReadable: "1.23.4 Revision: aa1f615c"}}})

	// test StopStreamRequest
	var stop public.StopStreamRequest
	testJSON = []byte(`{"id":"id","force":true}`)
	err = json.Unmarshal(testJSON, &stop)
	AssertTrue(t, err == nil)
	AssertEqual(t, stop.Id, media.StreamId("id"))
	AssertEqual(t, stop.Force, true)

	testCases := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{force":true}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"","force":true}`),
		},
		{
			name:      "nil force",
			testArray: []byte(`{"id":"id"}`),
		},
	}

	for _, obj := range testCases {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &stop))
		})
	}

	//  ScanFolderRequest
	scan := public.ScanFolderRequest{}
	testJSON = []byte(`{"directory":"directory","extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	AssertTrue(t, err == nil)
	AssertEqual(t, scan.Directory, "directory")
	AssertEqual(t, scan.Extensions, []string{"ext1", "ext2"})

	//  test directory by nil
	testJSON = []byte(`{"extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	AssertError(t, err)

	//  test directory by empty
	testJSON = []byte(`{"directory":"","extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &scan)
	AssertError(t, err)

	//  test extensions by nil
	testJSON = []byte(`{"directory":"directory"}`)
	err = json.Unmarshal(testJSON, &scan)
	AssertError(t, err)

	testJSON = []byte(`{"directory":"directory", "extensions":["",""]}`)
	err = json.Unmarshal(testJSON, &scan)
	AssertError(t, err)

	// type StartCachedStreamRequest
	testVariable1 := public.StartCachedStreamRequest{}
	testJSON = []byte(`{"id": "dwawf2"}`)
	err = json.Unmarshal(testJSON, &testVariable1)
	AssertNoError(t, err)
	AssertEqual(t, testVariable1.Id, media.StreamId("dwawf2"))

	testCases1 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"ids": "dwawf2"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id": ""}`),
		},
	}

	for _, obj := range testCases1 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable1))
		})
	}

	// type RestartStreamRequest
	testVariable2 := public.RestartStreamRequest{}
	testJSON = []byte(`{"id": "dwawf2"}`)
	err = json.Unmarshal(testJSON, &testVariable1)
	AssertNoError(t, err)
	AssertEqual(t, testVariable1.Id, media.StreamId("dwawf2"))

	testCases2 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"ids": "dwawf2"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id": ""}`),
		},
	}

	for _, obj := range testCases2 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable2))
		})
	}

	// type ProbeRequestIn
	testVariable3 := public.ProbeRequestIn{}
	testJSON = []byte(`{"url":{"id":23,"uri":"wdwd"}}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	AssertNoError(t, err)
	AssertEqual(t, testVariable3.Url, media.InputUri{InputUrl: media.InputUrl{Id: 23, Uri: "wdwd"}})

	testCases3 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil url",
			testArray: []byte(`{"udwrl":{"id":23,"uri":"wdwd"}}`),
		},
	}

	for _, obj := range testCases3 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable3))
		})
	}

	// type ProbeRequestOut
	testVariable4 := public.ProbeRequestOut{}
	testJSON = []byte(`{"url":{"id":23,"uri":"wdwd"}}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	AssertNoError(t, err)
	AssertEqual(t, testVariable4.Url, media.OutputUri{media.OutputUrl{Id: 23, Uri: "wdwd"}, media.OutputUriData{}})

	testCases4 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil url",
			testArray: []byte(`{"udwrl":{"id":23,"uri":"wdwd"}}`),
		},
	}

	for _, obj := range testCases4 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable4))
		})
	}

	// type ChangeInputStreamRequest
	testVariable5 := public.InjectMasterInputRequest{}
	testJSON = []byte(`{"id":"wd", "url":{"id":1, "uri":"wer"}}`)
	err = json.Unmarshal(testJSON, &testVariable5)
	AssertNoError(t, err)
	AssertEqual(t, testVariable5.Sid, media.StreamId("wd"))
	AssertEqual(t, testVariable5.Url, media.InputUri{media.InputUrl{Id: 1, Uri: "wer"}, media.InputUriData{}})

	testCases5 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"url":{"id":1, "uri":"wer"}}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "url":{"id":1, "uri":"wer"}}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wd", "url":{"id":, "uri":""}}`),
		},
	}

	for _, obj := range testCases5 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable5))
		})
	}

	// type ChangeInputStreamRequest
	testVariable6 := public.ChangeInputStreamRequest{}
	testJSON = []byte(`{"id":"wd", "channel_id": 1}`)
	err = json.Unmarshal(testJSON, &testVariable6)
	AssertNoError(t, err)
	AssertEqual(t, testVariable6.Sid, media.StreamId("wd"))
	AssertEqual(t, testVariable6.ChannelId, 1)

	testCases6 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"channel_id": 1}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "channel_id": 1}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wd", "channel_id": }`),
		},
	}

	for _, obj := range testCases6 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable6))
		})
	}

	// type RemoveMasterInputRequest
	testVariable7 := public.RemoveMasterInputRequest{}
	testJSON = []byte(`{"id":"wdw2", "url": {"id":1,"uri":"wf"}}`)
	err = json.Unmarshal(testJSON, &testVariable7)
	AssertNoError(t, err)
	AssertEqual(t, testVariable7.Sid, media.StreamId("wdw2"))
	AssertEqual(t, testVariable7.Url, media.InputUri{media.InputUrl{Id: 1, Uri: "wf"}, media.InputUriData{}})

	testCases7 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"url": {"id":1,"uri":"wf"}}`),
		},
		{
			name:      "invlaid id",
			testArray: []byte(`{"id":"", "url": {"id":1,"uri":"wf"}}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":"wdw2"}`),
		},
		{
			name:      "uri empty",
			testArray: []byte(`{"id":23, "uri": ""}`),
		},
	}

	for _, obj := range testCases7 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable7))
		})
	}

	// type OutputUrl
	testVariable8 := public.RemoveVideoRequest{}
	testJSON = []byte(`{"path":"wdwdw"}`)
	err = json.Unmarshal(testJSON, &testVariable8)
	AssertNoError(t, err)
	AssertEqual(t, testVariable8.Path, "wdwdw")

	testCases8 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"pgeath":"wdwdw"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"path":""}`),
		},
	}

	for _, obj := range testCases8 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable8))
		})
	}

	// type MountBucketRequest
	testVariable9 := public.MountBucketRequest{}
	testJSON = []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`)
	err = json.Unmarshal(testJSON, &testVariable9)
	AssertNoError(t, err)
	AssertEqual(t, testVariable9.Name, "vasya")
	AssertEqual(t, testVariable9.Path, "D:9wd1kw/wd")
	AssertEqual(t, testVariable9.Key, "keykey")
	AssertEqual(t, testVariable9.Secret, "ujkj92rj")

	testCases9 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil name",
			testArray: []byte(`{"path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty name",
			testArray: []byte(`{"name":"","path":"D:9wd1kw/wd","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil path",
			testArray: []byte(`{"name":"vasya","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty path",
			testArray: []byte(`{"name":"vasya","path":"","key":"keykey","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil key",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","secret":"ujkj92rj"}`),
		},
		{
			name:      "empty key",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"","secret":"ujkj92rj"}`),
		},
		{
			name:      "nil secret",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey",}`),
		},
		{
			name:      "empty secret",
			testArray: []byte(`{"name":"vasya","path":"D:9wd1kw/wd","key":"keykey","secret":""}`),
		},
	}

	for _, obj := range testCases9 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable9))
		})
	}

	// type UnmountBucketRequest
	testVariable10 := public.UnmountBucketRequest{}
	testJSON = []byte(`{"path":"C://wdqwd?wdw/qwf2"}`)
	err = json.Unmarshal(testJSON, &testVariable10)
	AssertNoError(t, err)
	AssertEqual(t, testVariable10.Path, "C://wdqwd?wdw/qwf2")

	testCases10 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil path",
			testArray: []byte(`{"uri": "qwer"}`),
		},
		{
			name:      "invlaid path",
			testArray: []byte(`{"id":-234, "uri": "qwer"}`),
		},
	}

	for _, obj := range testCases10 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable10))
		})
	}
}
