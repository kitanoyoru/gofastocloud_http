package gofastocloud_http

import (
	"encoding/base64"
	"encoding/json"
	"net/url"

	"github.com/gorilla/websocket"
	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_http/gofastocloud_http/public"
)

func (fasto *FastoCloud) Run(client public.WsClient) {
	u, err := url.Parse(fasto.generateRoute("/updates"))
	if err != nil {
		return
	}

	if fasto.Login != nil && fasto.Password != nil {
		token := base64.StdEncoding.EncodeToString([]byte(*fasto.Login + ":" + *fasto.Password))
		values := u.Query()

		values.Add("token", token)

		u.RawQuery = values.Encode()
	}

	if u.Scheme == "https" {
		u.Scheme = "wss"
	} else {
		u.Scheme = "ws"
	}

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		return
	}

	defer c.Close()

	for !fasto.isStopped {
		_, message, err := c.ReadMessage()
		if err != nil {
			break
		}

		err = fasto.processUpdate(message, client)
		if err != nil {
			break
		}
	}
}

func (fasto *FastoCloud) Stop() {
	fasto.isStopped = true
}

func (fasto *FastoCloud) processUpdate(rawMessage []byte, client public.WsClient) error {
	const kStatisticServiceBroadcastCommand = "statistic_service"
	const kQuitStatusStreamBroadcastCommand = "quit_status_stream"
	const kStatisticStreamBroadcastCommand = "statistic_stream"
	const kMlNotificationStreamBroadcastCommand = "ml_notification_stream"
	const kStatisticCDNBroadcastCommand = "statistic_cdn"

	var msg public.WSMessage

	err := json.Unmarshal(rawMessage, &msg)
	if err != nil {
		return err
	}

	if msg.Type == kStatisticServiceBroadcastCommand {
		var stat media.ServiceStatisticInfo

		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err

		}
		fasto.Statistics = &stat
	} else if msg.Type == kQuitStatusStreamBroadcastCommand {
		var quit media.QuitStatusInfo

		err := json.Unmarshal(msg.Data, &quit)
		if err != nil {
			return err
		}

		fasto.deleteActiveStream(quit.Id)
		client.OnStreamQuit(quit)
	} else if msg.Type == kStatisticStreamBroadcastCommand {
		var stat media.StreamStatisticInfo

		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}

		fasto.Streams = append(fasto.Streams, stat.Id)
		client.OnStreamStatistic(stat)
	} else if msg.Type == kMlNotificationStreamBroadcastCommand {
		var notif media.MlNotificationInfo

		err := json.Unmarshal(msg.Data, &notif)
		if err != nil {
			return err
		}

		client.OnStreamMlNotification(notif)
	} else if msg.Type == kStatisticCDNBroadcastCommand {
		var stat media.CDNStatisticInfo

		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
		client.OnCDNStatistic(stat)
	}

	return nil
}
