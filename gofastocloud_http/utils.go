package gofastocloud_http

import "gitlab.com/fastogt/gofastocloud/gofastocloud/media"

func (fasto *FastoCloud) deleteActiveStream(sid media.StreamId) {
	for i, id := range fasto.Streams {
		if id == sid {
			fasto.Streams = append(fasto.Streams[:i], fasto.Streams[i+1:]...)
			break
		}
	}
}
