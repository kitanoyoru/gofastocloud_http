package gofastocloud_http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_http/gofastocloud_http/public"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type FastoCloud struct {
	Endpoint string  // https://api.fastocloud.com
	Login    *string // fastocloud
	Password *string // fastocloud
	RealIP   *string // using in ott for proxing request

	Statistics *media.ServiceStatisticInfo
	Streams    []media.StreamId

	isStopped bool
}

func NewFastoCloud(endpoint string, login *string, pass *string) *FastoCloud {
	return NewRealIPFastoCloud(endpoint, login, pass, nil)
}

func NewRealIPFastoCloud(endpoint string, login *string, pass *string, realIP *string) *FastoCloud {
	return &FastoCloud{Endpoint: endpoint, Login: login, Password: pass, Statistics: nil, RealIP: realIP}
}

// version
func (fasto *FastoCloud) GetVersion() (*public.Version, error) {
	req, err := fasto.makeHttpGetRequest("/server/version")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.VersionResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// content
func (fasto *FastoCloud) GetContent() (*public.Content, error) {
	req, err := fasto.makeHttpGetRequest("/server/db/content")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ContentResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetLiveStreams() ([]json.RawMessage, error) {
	url := "/server/db/stream/live/list"
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.LiveStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return response.Data.Streams, nil
}

func (fasto *FastoCloud) GetLiveStream(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/stream/live/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetVodStream(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/stream/vod/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetEpisodeStream(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/stream/episode/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetCatchUp(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/stream/catchup/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.CatchUpResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) SendCatchUps(programs []media.ProgramInfo) (*json.RawMessage, error) {
	var reqParams = public.CatchUpRequest{
		Programs: programs,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/catchup", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.CatchUpResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetSeason(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/season/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.SerialResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetSerial(id string) (*json.RawMessage, error) {
	url := fmt.Sprintf("/server/db/serial/%s", id)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.SerialResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// inc view count
func (fasto *FastoCloud) IncStreamView(sid string) (*public.IncStreamView, error) {
	req, err := fasto.makeHttpPostRequest(fmt.Sprintf("/server/db/stream/view/%s", sid), "application/json", nil)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.IncStreamViewResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// upload file
func (fasto *FastoCloud) UploadFile(path string) (*public.UploadFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
	if err != nil {
		return nil, err
	}

	io.Copy(part, file)
	writer.Close()
	return fasto.uploadFileFromReader(writer.FormDataContentType(), body)
}

// bucket
func (fasto *FastoCloud) MountBucket(name, path, key, secret string) (*public.Bucket, error) {
	var reqParams = public.MountBucketRequest{
		Name:   name,
		Path:   path,
		Key:    key,
		Secret: secret,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/s3bucket/mount", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) UnmountBucket(path string) (*public.Bucket, error) {

	var reqParams = public.UnmountBucketRequest{
		Path: path,
	}
	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/s3bucket/unmount", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetMountedBuckets() (*public.GetBucket, error) {
	req, err := fasto.makeHttpGetRequest("/media/s3bucket/list")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.GetBucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// probe
func (fasto *FastoCloud) ProbeIn(url media.InputUri) (*public.Probe, error) {
	var reqParams = public.ProbeRequestIn{
		Url: url,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/probe_in", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ProbeResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) ProbeOut(url media.OutputUri) (*public.Probe, error) {
	var reqParams = public.ProbeRequestOut{
		Url: url,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/probe_out", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ProbeResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) CatchUp(programs []media.ProgramInfo) (*json.RawMessage, error) {
	var reqParams = public.CatchUpRequest{
		Programs: programs,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/stream/catchup", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.CatchUpResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// stream
func (fasto *FastoCloud) StartStream(config []byte) (*public.StartStream, error) {
	req, err := fasto.makeHttpPostRequest("/media/stream/start", "application/json", bytes.NewBuffer(config))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) StopStream(sid media.StreamId, force bool) (*public.StopStream, error) {
	var reqParams = public.StopStreamRequest{
		Id:    sid,
		Force: force,
	}

	params, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/stop", "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.StopStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) CleanStream(config []byte) (*public.CleanStream, error) {
	req, err := fasto.makeHttpPostRequest("/media/stream/clean", "application/json", bytes.NewBuffer(config))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.CleanStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) StreamConfig(sid media.StreamId) (*public.StreamConfig, error) {
	req, err := fasto.makeHttpGetRequest(fmt.Sprintf("/media/stream/config/%s", sid))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamConfigResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// restart
func (fasto *FastoCloud) RestartStream(sid media.StreamId) (*public.RestartStream, error) {
	var reqParams = public.RestartStreamRequest{
		Id: sid,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/restart", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.RestartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

// Change input source
func (fasto *FastoCloud) ChangeInputSource(sid media.StreamId, channelId int) (*public.ChangeInputStream, error) {
	var reqParams = public.ChangeInputStreamRequest{
		Sid:       sid,
		ChannelId: channelId,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/change_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response public.ChangeInputStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

// stats
func (fasto *FastoCloud) GetServerStats() (*public.FullStatService, error) {
	req, err := fasto.makeHttpGetRequest("/media/stats")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var stat public.StatServiceResponse
	err = decoder.Decode(&stat)
	if err != nil {
		return nil, err
	}
	return &stat.Data, nil
}

func (fasto *FastoCloud) GetStreamStats(sid media.StreamId) (*public.StreamStatistic, error) {
	req, err := fasto.makeHttpGetRequest(fmt.Sprintf("/media/stream/stats/%s", sid))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)

	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamStatisticResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetStreamsStats() (*public.StreamsStatistic, error) {
	req, err := fasto.makeHttpGetRequest("/media/streams_stats")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamsStatisticResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// video
func (fasto *FastoCloud) RemoveVideo(path string) (*public.RemoveVideo, error) {
	var reqParams = public.RemoveVideoRequest{
		Path: path,
	}

	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/server/video/remove", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.RemoveVideoResponce
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// sync
func (fasto *FastoCloud) SyncConfigs(config []json.RawMessage) (*public.SyncConfig, error) {
	var reqParams = public.SyncConfigRequest{
		Configs: config,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/sync", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.SyncConfigResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// configs
func (fasto *FastoCloud) GetConfigs() (*public.StreamConfigs, error) {
	req, err := fasto.makeHttpGetRequest("/media/streams_configs")
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.StreamConfigsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}
	return &response.Data, nil
}

// folder
func (fasto *FastoCloud) ScanFolder(directory string, extensions []string) (*public.ScanFolder, error) {
	var reqParams = public.ScanFolderRequest{
		Directory:  directory,
		Extensions: extensions,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/folder/scan", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.ScanFolderResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// Master Source Stream
func (fasto *FastoCloud) InjectMasterSourceStream(sid media.StreamId, url media.InputUri) (*public.InjectMasterInput, error) {
	var reqParams = public.InjectMasterInputRequest{
		Sid: sid,
		Url: url,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/inject_master_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.InjectMasterInputResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) RemoveMasterSourceStream(sid media.StreamId, url media.InputUri) (*public.RemoveMasterInput, error) {
	var reqParams = public.RemoveMasterInputRequest{
		Sid: sid,
		Url: url,
	}
	param, err := json.Marshal(reqParams)
	if err != nil {
		return nil, err
	}

	req, err := fasto.makeHttpPostRequest("/media/stream/remove_master_source", "application/json", bytes.NewBuffer(param))
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}
	var response public.RemoveMasterInputResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// private:

func (fasto *FastoCloud) generateRoute(path string) string {
	return fasto.Endpoint + path
}

func (fasto *FastoCloud) makeHttpPostRequest(route string, contentType string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("POST", fasto.generateRoute(route), body)
	if err != nil {
		return nil, err
	}

	if fasto.RealIP != nil {
		req.Header.Set("X-Real-IP", *fasto.RealIP)
	}

	req.Header.Add("Content-Type", contentType)
	if fasto.Login != nil && fasto.Password != nil {
		req.SetBasicAuth(*fasto.Login, *fasto.Password)
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpGetRequest(route string) (*http.Request, error) {
	req, err := http.NewRequest("GET", fasto.generateRoute(route), nil)
	if err != nil {
		return nil, err
	}

	if fasto.RealIP != nil {
		req.Header.Set("X-Real-IP", *fasto.RealIP)
	}

	if fasto.Login != nil && fasto.Password != nil {
		req.SetBasicAuth(*fasto.Login, *fasto.Password)
	}

	return req, nil
}

func (fasto *FastoCloud) uploadFileFromReader(contentType string, reader io.Reader) (*public.UploadFile, error) {
	req, err := fasto.makeHttpPostRequest("/server/video/upload", contentType, reader)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if resp.StatusCode != http.StatusOK {
		var eresponse gofastogt.ErrorResponse
		err = decoder.Decode(&eresponse)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("wrong response status: %d, error: %v", resp.StatusCode, eresponse.Error)
	}

	var response public.UploadFileResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func checkErrorResponse(decoder *json.Decoder, response *http.Response) error {
	if response.StatusCode != http.StatusOK {
		var eresponse gofastogt.ErrorResponse
		err := decoder.Decode(&eresponse)
		if err != nil {
			return err
		}
		return fmt.Errorf("wrong response status: %d, error: %v", response.StatusCode, eresponse.Error)
	}
	return nil
}
