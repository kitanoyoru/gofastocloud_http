package public

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Get hardware hash

type GetHardwareHashRequest struct {
	Algo gofastogt.AlgoType `json:"algo"`
	Host string             `json:"host"`
}

type HardwareHash struct {
	Key string `json:"key"`
}

type HardwareHashResponse struct {
	Data HardwareHash `json:"data"`
}

func (hash *GetHardwareHashRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Algo *gofastogt.AlgoType `json:"algo"`
		Host *string             `json:"host"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Algo == nil {
		return errors.New("algo field required")
	}
	if request.Host == nil {
		return errors.New("host field required")
	}

	hash.Algo = *request.Algo
	hash.Host = *request.Host
	return nil
}

// Upload file

type UploadFile struct {
	Info media.MediaUrlInfo `json:"info"`
	Path string             `json:"path"`
}

type UploadFileResponse struct {
	Data UploadFile `json:"data"`
}

// Start stream

type StartStream struct {
}

type StartStreamResponse struct {
	Data StartStream `json:"data"`
}

// Stop stream

type StopStreamRequest struct {
	Id    media.StreamId `json:"id"`
	Force bool           `json:"force"`
}

type StopStream struct {
}

type StopStreamResponse struct {
	Data StopStream `json:"data"`
}

func (stop *StopStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Id    *media.StreamId `json:"id"`
		Force *bool           `json:"force"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Id == nil {
		return errors.New("id field required")
	}

	if len(*request.Id) == 0 {
		return errors.New("id can't be empty")
	}

	if request.Force == nil {
		return errors.New("force field required")
	}
	stop.Id = *request.Id
	stop.Force = *request.Force
	return nil
}

// Start cahced stream

type StartCachedStreamRequest struct {
	Id media.StreamId `json:"id"`
}

type StartCachedStream struct {
}

type StartCachedStreamResponse struct {
	Data StartCachedStream `json:"data"`
}

func (start *StartCachedStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Id *media.StreamId `json:"id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Id == nil {
		return errors.New("id field required")
	}
	if len(*request.Id) == 0 {
		return errors.New("id can't be empty")
	}
	start.Id = *request.Id
	return nil
}

// Restart stream

type RestartStreamRequest struct {
	Id media.StreamId `json:"id"`
}

type RestartStream struct {
}

type RestartStreamResponse struct {
	Data RestartStream `json:"data"`
}

func (restart *RestartStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Id *media.StreamId `json:"id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Id == nil {
		return errors.New("id field required")
	}
	if len(*request.Id) == 0 {
		return errors.New("id can't be empty")
	}
	restart.Id = *request.Id
	return nil
}

// Clean stream

type CleanStream struct {
}

type CleanStreamResponse struct {
	Data CleanStream `json:"data"`
}

// Inc view stream

type IncStreamView struct {
}

type IncStreamViewResponse struct {
	Data IncStreamView `json:"data"`
}

// Version service

type Version struct {
	Version VersionStruct `json:"version"`
}
type VersionStruct struct {
	Short         string `json:"short"`
	HumanReadable string `json:"human_readable"`
}

type VersionResponse struct {
	Data Version `json:"data"`
}

// Content

type Content struct {
	Streams  []json.RawMessage `json:"streams"`
	Vods     []json.RawMessage `json:"vods"`
	Serials  []json.RawMessage `json:"serials"`
	Episodes []json.RawMessage `json:"episodes"`
	Seasons  []json.RawMessage `json:"seasons"`
	Catchups []json.RawMessage `json:"catchups"`
}

type ContentResponse struct {
	Data Content `json:"data"`
}

type StreamResponse struct {
	Data json.RawMessage `json:"data"`
}
type SerialResponse struct {
	Data json.RawMessage `json:"data"`
}

// Catch up

type CatchUpRequest struct {
	Programs []media.ProgramInfo
}

type CatchUpResponse struct {
	Data json.RawMessage `json:"data"`
}

// Service stats

type FullStatService struct {
	media.ServiceStatisticInfo
	Status     gofastocloud_base.ConnectionStatus `json:"status"`
	Version    string                             `json:"version"`
	Project    string                             `json:"project"`
	ExpireTime gofastogt.UtcTimeMsec              `json:"expiration_time"`
	OS         gofastocloud_base.OperationSystem  `json:"os"`
}

func NewFullStatService(stat media.ServiceStatisticInfo, status gofastocloud_base.ConnectionStatus, version string, project string, exp gofastogt.UtcTimeMsec, os gofastocloud_base.OperationSystem) *FullStatService {
	return &FullStatService{stat, status, version, project, exp, os}
}

type StatServiceResponse struct {
	Data FullStatService `json:"data"`
}

// Probe
type ProbeRequestIn struct {
	Url media.InputUri `json:"url"`
}

type ProbeRequestOut struct {
	Url media.OutputUri `json:"url"`
}

type Probe struct {
	Probe media.ProbeData    `json:"probe"`
	Info  media.MediaUrlInfo `json:"info"`
}

type ProbeResponse struct {
	Data Probe `json:"data"`
}

func (requestIn *ProbeRequestIn) UnmarshalJSON(data []byte) error {
	request := struct {
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Url == nil {
		return errors.New("url field required")
	}

	requestIn.Url = *request.Url
	return nil
}

func (requestOut *ProbeRequestOut) UnmarshalJSON(data []byte) error {
	request := struct {
		Url *media.OutputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Url == nil {
		return errors.New("url field required")
	}

	requestOut.Url = *request.Url
	return nil

}

// Scan folder

type ScanFolderRequest struct {
	Directory  string   `json:"directory"`
	Extensions []string `json:"extensions"`
}

type ScanFolder struct {
	Files []string `json:"files"`
}

type ScanFolderResponse struct {
	Data ScanFolder `json:"data"`
}

func (scan *ScanFolderRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Directory  *string   `json:"directory"`
		Extensions *[]string `json:"extensions"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Directory == nil {
		return errors.New("directory field required")
	}
	if len(*request.Directory) == 0 {
		return errors.New("directory can't be empty")
	}
	if request.Extensions == nil {
		return errors.New("extensions field required")
	}
	for _, str := range *request.Extensions {
		if len(str) == 0 {
			return errors.New("extensions can't be empty")
		}
	}

	scan.Directory = *request.Directory
	scan.Extensions = *request.Extensions
	return nil
}

// Stream statistic

type StreamStatistic struct {
	Stats *media.StreamStatisticInfo `json:"statistic"`
}

type StreamStatisticResponse struct {
	Data StreamStatistic `json:"data"`
}

// Live Streams

type LiveStreamList struct {
	Streams []json.RawMessage `json:"streams"`
}

type LiveStreamResponse struct {
	Data LiveStreamList `json:"data"`
}

// Streams config

type StreamConfig struct {
	Config json.RawMessage `json:"config"`
}

type StreamConfigResponse struct {
	Data StreamConfig `json:"data"`
}

// Streams statistics

type StreamsStatistic struct {
	Stats []*media.StreamStatisticInfo `json:"statistics"`
}

type StreamsStatisticResponse struct {
	Data StreamsStatistic `json:"data"`
}

// Stream configs

type StreamConfigs struct {
	Configs []json.RawMessage `json:"configs"`
}

type StreamConfigsResponse struct {
	Data StreamConfigs `json:"data"`
}

// Change input stream
type ChangeInputStreamRequest struct {
	Sid       media.StreamId `json:"id"`
	ChannelId int            `json:"channel_id"`
}

type ChangeInputStream struct {
}

type ChangeInputStreamResponse struct {
	Data ChangeInputStream `json:"data"`
}

func (change *ChangeInputStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid       *media.StreamId `json:"id"`
		ChannelId *int            `json:"channel_id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.ChannelId == nil {
		return errors.New("channel_id field required")
	}

	change.Sid = *request.Sid
	change.ChannelId = *request.ChannelId
	return nil
}

// Master sourse stream
type InjectMasterInputRequest struct {
	Sid media.StreamId `json:"id"`
	Url media.InputUri `json:"url"`
}

type InjectMasterInput struct {
}

type InjectMasterInputResponse struct {
	Data InjectMasterInput `json:"data"`
}

type RemoveMasterInputRequest struct {
	Sid media.StreamId `json:"id"`
	Url media.InputUri `json:"url"`
}

type RemoveMasterInput struct {
}

type RemoveMasterInputResponse struct {
	Data RemoveMasterInput `json:"data"`
}

func (injectMaster *InjectMasterInputRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid *media.StreamId `json:"id"`
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.Url == nil {
		return errors.New("url field required")
	}
	injectMaster.Sid = *request.Sid
	injectMaster.Url = *request.Url
	return nil
}

func (removeMaster *RemoveMasterInputRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid *media.StreamId `json:"id"`
		Url *media.InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.Url == nil {
		return errors.New("url field required")
	}
	removeMaster.Sid = *request.Sid
	removeMaster.Url = *request.Url
	return nil
}

// Revome video

type RemoveVideoRequest struct {
	Path string `json:"path"`
}

type RemoveVideo struct {
}

type RemoveVideoResponce struct {
	Data RemoveVideo `json:"data"`
}

func (change *RemoveVideoRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Path *string `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	change.Path = *request.Path
	return nil
}

// Sync configs
type SyncConfigRequest struct {
	Configs []json.RawMessage `json:"configs"`
}
type SyncConfig struct {
}

type SyncConfigResponse struct {
	Data SyncConfig `json:"data"`
}

func (syncConfig *SyncConfigRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Configs *[]json.RawMessage `json:"configs"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Configs == nil {
		return errors.New("path field required")
	}
	syncConfig.Configs = *request.Configs
	return nil
}

// Bucket
type MountBucketRequest struct {
	Name   string `json:"name"`
	Path   string `json:"path"`
	Key    string `json:"key"`
	Secret string `json:"secret"`
}

type UnmountBucketRequest struct {
	Path string `json:"path"`
}

type Bucket struct {
}

type BucketResponse struct {
	Data Bucket `json:"data"`
}

func (mountBucket *MountBucketRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Name   *string `json:"name"`
		Path   *string `json:"path"`
		Key    *string `json:"key"`
		Secret *string `json:"secret"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("name field required")
	}
	if len(*request.Name) == 0 {
		return errors.New("name can't be empty")
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	if request.Key == nil {
		return errors.New("key field required")
	}
	if len(*request.Key) == 0 {
		return errors.New("key can't be empty")
	}
	if request.Secret == nil {
		return errors.New("secret field required")
	}
	if len(*request.Secret) == 0 {
		return errors.New("secret can't be empty")
	}
	mountBucket.Name = *request.Name
	mountBucket.Path = *request.Path
	mountBucket.Key = *request.Key
	mountBucket.Secret = *request.Secret
	return nil
}

func (unmountBucket *UnmountBucketRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Path *string `json:"path"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Path == nil {
		return errors.New("path field required")
	}
	if len(*request.Path) == 0 {
		return errors.New("path can't be empty")
	}
	unmountBucket.Path = *request.Path
	return nil
}

//Get Buckets

type Buckets struct {
	Path string `json:"path"`
	Name string `json:"name"`
}

type GetBucket struct {
	Bucket []Buckets `json:"data"`
}
type GetBucketResponse struct {
	Data GetBucket `json:"data"`
}
