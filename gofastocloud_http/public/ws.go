package public

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
)

type WsClient interface {
	OnStreamMlNotification(notif media.MlNotificationInfo)
	OnStreamStatistic(stat media.StreamStatisticInfo)
	OnStreamQuit(quit media.QuitStatusInfo)
	OnCDNStatistic(cdn media.CDNStatisticInfo)
}

type WSMessage struct {
	Type string          `json:"type"`
	Data json.RawMessage `json:"data"`
}
